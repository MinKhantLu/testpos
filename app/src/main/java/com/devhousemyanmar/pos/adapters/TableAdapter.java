package com.devhousemyanmar.pos.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devhousemyanmar.pos.R;
import com.devhousemyanmar.pos.models.Table;

import java.util.ArrayList;

/**
 * Created by minkhantlu on 9/20/18.
 */

public class TableAdapter extends RecyclerView.Adapter<TableAdapter.TableViewHolder> {

    Context context;
    ClickTable clickTable;
    ArrayList<Table> tables = new ArrayList<>();

    public TableAdapter(Context context, ClickTable clickTable, ArrayList<Table> tables) {
        this.context = context;
        this.clickTable = clickTable;
        this.tables = tables;
    }


    @NonNull
    @Override
    public TableViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_table, parent, false);
        return new TableViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TableViewHolder holder, int position) {
        if (tables.get(position).isOccupied == 0) {
            holder.cardTable.setCardBackgroundColor(context.getResources().getColor(R.color.available));
        } else {
            holder.cardTable.setCardBackgroundColor(context.getResources().getColor(R.color.occupied));
        }

        holder.tvTableNo.setText(tables.get(position).name);
    }

    @Override
    public int getItemCount() {
        return tables.size();
    }

    public class TableViewHolder extends RecyclerView.ViewHolder {
        CardView cardTable;
        TextView tvTableNo;

        public TableViewHolder(View itemView) {
            super(itemView);
            cardTable = itemView.findViewById(R.id.cv_table);
            tvTableNo = itemView.findViewById(R.id.tv_table_no);

            cardTable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickTable.onClickTable(tables.get(getAdapterPosition()));
                }
            });
        }
    }

    public interface ClickTable {
        void onClickTable(Table table);
    }
}
