package com.devhousemyanmar.pos.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devhousemyanmar.pos.R;

import java.util.ArrayList;

/**
 * Created by minkhantlu on 9/21/18.
 */

public class TaxAdapter extends RecyclerView.Adapter<TaxAdapter.TaxViewHolder> {

    Context context;
    ClickTax clickTax;
    private ArrayList<Boolean> isSelected = new ArrayList<>();

    public TaxAdapter(Context context) {
        this.context = context;

        isSelected.clear();
        isSelected.add(false);
        isSelected.add(false);
        isSelected.add(false);
        isSelected.add(false);
    }

    @NonNull
    @Override
    public TaxViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tax, parent, false);
        return new TaxViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TaxViewHolder holder, int position) {
        if (isSelected.get(position)) {
            holder.cvTaxMain.setCardBackgroundColor(context.getResources().getColor(R.color.green_300));
        } else {
            holder.cvTaxMain.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        }

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class TaxViewHolder extends RecyclerView.ViewHolder {
        CardView cvTaxMain;

        public TaxViewHolder(View itemView) {
            super(itemView);
            cvTaxMain = itemView.findViewById(R.id.cv_tax_main);
            cvTaxMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Boolean flag = isSelected.get(getAdapterPosition());
                    isSelected.remove(getAdapterPosition());
                    isSelected.add(getAdapterPosition(), !flag);
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }

    public interface ClickTax {
        void onClickTax(int position);
    }
}
