package com.devhousemyanmar.pos.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devhousemyanmar.pos.R;
import com.devhousemyanmar.pos.models.Product;

import java.util.ArrayList;

import me.grantland.widget.AutofitHelper;

/**
 * Created by minkhantlu on 9/19/18.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.TextViewHolder> {

    Context context;
    ClickProduct clickProduct;
    private ArrayList<Product> productArrayList = new ArrayList<>();

    public ProductAdapter(Context context, ClickProduct clickProduct, ArrayList<Product> productArrayList) {
        this.context = context;
        this.clickProduct = clickProduct;
        this.productArrayList = productArrayList;
    }

    @NonNull
    @Override
    public TextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new TextViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TextViewHolder holder, int position) {
        holder.tvPrice.setText(productArrayList.get(position).price+"");
        holder.tvName.setText(productArrayList.get(position).name);

//        Picasso.get().load(MainApi.BASE_API + productArrayList.get(position).image_path).into(holder.imageView);
        AutofitHelper.create(holder.tvName);
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    public class TextViewHolder extends RecyclerView.ViewHolder {
        CardView cvProduct;
        ImageView imageView;
        TextView tvName, tvPrice;

        public TextViewHolder(View itemView) {
            super(itemView);
            cvProduct = itemView.findViewById(R.id.cv_product);
            imageView = itemView.findViewById(R.id.img_product_home);
            tvName = itemView.findViewById(R.id.tv_pname_home);
            tvPrice = itemView.findViewById(R.id.tv_pprice_home);

            cvProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickProduct.onClick(productArrayList.get(getAdapterPosition()));
                }
            });
        }
    }

    public interface ClickProduct {
        public void onClick(Product product);
    }
}
