package com.devhousemyanmar.pos.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by minkhantlu on 9/19/18.
 */

public class TabViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<String> titleList = new ArrayList<>();
    private ArrayList<Fragment> fragmentArrayList = new ArrayList<>();

    public
    TabViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment,String title){
        fragmentArrayList.add(fragment);
        titleList.add(title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentArrayList.get(position);
    }


    @Override
    public int getCount() {
        return titleList.size();
    }
}
