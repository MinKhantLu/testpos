package com.devhousemyanmar.pos.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devhousemyanmar.pos.R;

import java.util.ArrayList;

/**
 * Created by minkhantlu on 9/21/18.
 */

public class CheckOutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public ArrayList<String> extraCharge = new ArrayList<>();
    private Context context;

    public CheckOutAdapter(Context context) {
        this.context = context;
        extraCharge.add("Sub Total");
        extraCharge.add("Service Charge");
        extraCharge.add("Gov Charge");
        extraCharge.add("Total");
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType > 20) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_extra_charge, parent, false);
            return new ChargeViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_check_out, parent, false);
            return new CheckOutViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) > 5) {
//            ChargeViewHolder chargeViewHolder = (ChargeViewHolder) holder;
//            chargeViewHolder.tvChargeName.setText(extraCharge.get(position - 5));
            CheckOutViewHolder checkOutViewHolder = (CheckOutViewHolder) holder;
            checkOutViewHolder.tvProduct.setText(extraCharge.get(position - 5));
            checkOutViewHolder.tvNo.setVisibility(View.INVISIBLE);
            checkOutViewHolder.tvUPrice.setVisibility(View.INVISIBLE);
            checkOutViewHolder.tvQty.setText("5%");
            if (getItemViewType(position) == 4 + extraCharge.size()) {
                checkOutViewHolder.layout.setBackgroundColor(context.getResources().getColor(R.color.grey_300));
            } else {
                checkOutViewHolder.layout.setBackgroundColor(context.getResources().getColor(R.color.grey_200));
            }


        } else {
            CheckOutViewHolder checkOutViewHolder = (CheckOutViewHolder) holder;
            checkOutViewHolder.tvNo.setText((position + 1) + "");
        }
    }

    public void addPromo(String promo) {
        extraCharge.add(extraCharge.size() - 1, promo);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return 5 + extraCharge.size();
    }

    public class CheckOutViewHolder extends RecyclerView.ViewHolder {
        TextView tvNo, tvProduct, tvUPrice, tvQty, tvTotalP;
        LinearLayout layout;

        public CheckOutViewHolder(View itemView) {
            super(itemView);
            tvNo = itemView.findViewById(R.id.tv_no);
            tvProduct = itemView.findViewById(R.id.tv_pname);
            tvUPrice = itemView.findViewById(R.id.tv_unit_price);
            tvQty = itemView.findViewById(R.id.tv_qty);
            tvTotalP = itemView.findViewById(R.id.tv_total_price);
            layout = itemView.findViewById(R.id.layout_main);
        }
    }

    public class ChargeViewHolder extends RecyclerView.ViewHolder {
        TextView tvChargeName, tvPercentage, tvPrice;

        public ChargeViewHolder(View itemView) {
            super(itemView);
            tvChargeName = itemView.findViewById(R.id.tv_charge_name);
            tvPercentage = itemView.findViewById(R.id.tv_percent);
            tvPrice = itemView.findViewById(R.id.tv_price);

        }
    }
}
