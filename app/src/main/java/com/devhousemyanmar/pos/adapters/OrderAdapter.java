package com.devhousemyanmar.pos.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.devhousemyanmar.pos.R;
import com.devhousemyanmar.pos.models.Order;

import java.util.ArrayList;

/**
 * Created by minkhantlu on 9/20/18.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {

    Context context;
    ClickOrder clickOrder;
    ArrayList<Order> orderArrayList = new ArrayList<>();

    public OrderAdapter(Context context, ClickOrder clickOrder, ArrayList<Order> orderArrayList) {
        this.context = context;
        this.clickOrder = clickOrder;
        this.orderArrayList = orderArrayList;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_confirm_order, parent, false);
        return new OrderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        holder.tvNo.setText((position + 1) + "");
        holder.tvName.setText(orderArrayList.get(position).name);
        holder.tvQuality.setText(orderArrayList.get(position).quality+"");
    }

    @Override
    public int getItemCount() {
        return orderArrayList.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {
        TextView tvNo, tvName, tvQuality;
        LinearLayout layout;

        public OrderViewHolder(View itemView) {
            super(itemView);
            tvNo = itemView.findViewById(R.id.tv_no);
            tvName = itemView.findViewById(R.id.tv_pname);
            tvQuality = itemView.findViewById(R.id.tv_quality);
            layout = itemView.findViewById(R.id.layout_order);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickOrder.onClickOrder(getAdapterPosition());
                }
            });
        }
    }

    public interface ClickOrder {
        void onClickOrder(int position);
    }
}
