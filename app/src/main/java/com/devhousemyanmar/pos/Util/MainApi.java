package com.devhousemyanmar.pos.Util;

/**
 * Created by minkhantlu on 9/25/18.
 */

public class MainApi {

    public static String BASE_API = "http://192.168.0.109:8888/restaurant_monitor/web/";
    public static String MAIN_API = BASE_API+"api/";
    public static String GET_PRODUCT = MAIN_API+"products";
    public static String GET_PRODUCT_BY_ID = MAIN_API+"product";
    public static String GET_CATEGORY = MAIN_API+"categories";
    public static String GET_TABLE  = MAIN_API+"tables";
    public static String LOGIN = MAIN_API+"login";

}
