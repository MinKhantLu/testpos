package com.devhousemyanmar.pos.models;

/**
 * Created by minkhantlu on 9/27/18.
 */

public class Table {
    public int id;
    public int isOccupied;
    public String name;

    public Table(int id, int isOccupied, String name) {
        this.id = id;
        this.isOccupied = isOccupied;
        this.name = name;
    }
}
