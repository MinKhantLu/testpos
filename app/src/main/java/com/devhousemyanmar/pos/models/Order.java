package com.devhousemyanmar.pos.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by minkhantlu on 9/27/18.
 */

public class Order extends RealmObject {
    @PrimaryKey
    public int id;
    public String name;
    public int quality;
    public String note;
}
