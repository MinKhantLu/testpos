package com.devhousemyanmar.pos.models;

import io.realm.RealmObject;

/**
 * Created by minkhantlu on 9/26/18.
 */

public class User extends RealmObject {
    public int id;
    public String name;
    public String role;
    public String address;
}
