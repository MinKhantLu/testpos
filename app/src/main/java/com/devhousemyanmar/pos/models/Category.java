package com.devhousemyanmar.pos.models;

/**
 * Created by minkhantlu on 9/26/18.
 */

public class Category {
    public int id;
    public String name;
    public Long created_at;
    public Long updated_at;

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
