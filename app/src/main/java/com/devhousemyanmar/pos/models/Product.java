package com.devhousemyanmar.pos.models;

/**
 * Created by minkhantlu on 9/25/18.
 */

public class Product {

    public int id;
    public String name;
    public String image_path;
    public int price;
    public Boolean status;
    public Long created_at;

    public Product() {
    }

    public Product(int id, String name, String image_path, int price, Boolean status) {
        this.id = id;
        this.name = name;
        this.image_path = image_path;
        this.price = price;
        this.status = status;
    }
}
