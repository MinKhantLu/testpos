package com.devhousemyanmar.pos.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.devhousemyanmar.pos.R;
import com.devhousemyanmar.pos.Util.MainApi;
import com.devhousemyanmar.pos.adapters.ProductAdapter;
import com.devhousemyanmar.pos.models.Order;
import com.devhousemyanmar.pos.models.Product;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;


public class HomeFragment extends Fragment implements ProductAdapter.ClickProduct {

    private static final String ARG_CAT_ID = "cat_id";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int paramCatId;

    private ProductAdapter productAdapter;
    private RecyclerView recyclerView;

    private ArrayList<Product> productArrayList = new ArrayList<>();
    private ArrayList<Order> order = new ArrayList<>();


    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance(int cat_id) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CAT_ID, cat_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            paramCatId = getArguments().getInt(ARG_CAT_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);


        //        getProduct(paramCatId);

        //adding DemoData
        for (int i = 0; i < 10; i++) {
            Product product = new Product(i, "Test Food", "link", 1000, true);
            productArrayList.add(product);
        }
        //end of adding

        recyclerView = v.findViewById(R.id.rv_home);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        productAdapter = new ProductAdapter(getContext(), this, productArrayList);
        recyclerView.setAdapter(productAdapter);
        return v;
    }

    @Override
    public void onClick(final Product product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_add_product, null, false);
        ImageView imgCover = v.findViewById(R.id.img_dialog);
        TextView tvName = v.findViewById(R.id.tv_pname);
        ImageView imgAdd = v.findViewById(R.id.img_add);
        ImageView imgSub = v.findViewById(R.id.img_sub);
        final EditText etNote = v.findViewById(R.id.et_note);
        final TextView txtQuality = v.findViewById(R.id.txt_quality);

        tvName.setText(product.name);
//        Picasso.get().load(MainApi.BASE_API + product.image_path).into(imgCover);

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quality = Integer.parseInt(txtQuality.getText().toString());
                quality++;
                txtQuality.setText(Integer.toString(quality));
            }
        });

        imgSub.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                txtQuality.setText("1");
                return false;
            }
        });

        imgSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quality = Integer.parseInt(txtQuality.getText().toString());
                if (quality > 1) {
                    quality--;
                    txtQuality.setText(Integer.toString(quality));
                }

            }
        });


        builder.setView(v);
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                int quality = Integer.parseInt(txtQuality.getText().toString());
                String note = etNote.getText().toString();

                //add each item to offline data
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                Order order = realm.createObject(Order.class, product.id);
                order.name = product.name;
                order.quality = quality;
                order.note = note;
                realm.commitTransaction();
                //end of adding...

                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getProduct(int catId) {
        AndroidNetworking.get(MainApi.GET_PRODUCT_BY_ID)
                .addHeaders("Authorization", "Bearer Fyt_5m1Jx9XfXHAgAdJ2VU54RCMA3QUj")
                .addQueryParameter("categoryId", catId + "")
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("products");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                Product product = new Product();
                                product.id = obj.getInt("id");
                                product.name = obj.getString("name");
                                product.image_path = obj.getString("image_path");
                                product.price = obj.getInt("price");
                                product.status = obj.getBoolean("status");
                                product.created_at = obj.getLong("created_at");
                                productArrayList.add(product);
                            }
                            productAdapter.notifyDataSetChanged();

                        } catch (Exception e) {

                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }
}
