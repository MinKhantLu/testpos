package com.devhousemyanmar.pos;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by minkhantlu on 9/28/18.
 */

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
