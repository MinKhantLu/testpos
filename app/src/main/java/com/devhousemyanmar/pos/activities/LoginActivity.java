package com.devhousemyanmar.pos.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.devhousemyanmar.pos.R;
import com.devhousemyanmar.pos.Util.MainApi;
import com.devhousemyanmar.pos.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin = findViewById(R.id.btn_login);
        realm = Realm.getDefaultInstance();


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                login("1", "ggwp");
                startActivity(new Intent(LoginActivity.this, TableActivity.class));
                finish();

            }
        });
    }

    public void login(String name, String password) {
        AndroidNetworking.post(MainApi.LOGIN)
                .addBodyParameter("userId", name)
                .addBodyParameter("password", password)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equals("200")) {
                                String token = jsonObject.getString("token");
                                JSONObject userObj = jsonObject.getJSONObject("user");
                                realm.beginTransaction();
                                User user = realm.createObject(User.class);
                                user.id = userObj.getInt("id");
                                user.name = userObj.getString("name");
                                user.role = userObj.getString("role");
                                user.address = userObj.getString("address");
                                realm.commitTransaction();
                                startActivity(new Intent(LoginActivity.this, TableActivity.class));
                                finish();
                            } else {

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}
