package com.devhousemyanmar.pos.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.devhousemyanmar.pos.R;
import com.devhousemyanmar.pos.Util.MainApi;
import com.devhousemyanmar.pos.adapters.TabViewPagerAdapter;
import com.devhousemyanmar.pos.fragments.HomeFragment;
import com.devhousemyanmar.pos.models.Category;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TabViewPagerAdapter adapter;
    private ArrayList<Category> categoryArrayList = new ArrayList<>();
    private int tableId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        Bundle bundle = getIntent().getExtras();
        String title = bundle.getString("title");
        tableId = bundle.getInt("id");


        getSupportActionBar().setTitle(title);

        tabLayout = findViewById(R.id.tab_main);
        viewPager = findViewById(R.id.vp_main);

        //adding DemoData
        String[] nameStr = {"Drink", "Food", "Meal"};
        adapter = new TabViewPagerAdapter(getSupportFragmentManager());
        for (int i = 0; i < 3; i++) {
            Category category = new Category(i, nameStr[i]);
            categoryArrayList.add(category);
            adapter.addFragment(HomeFragment.newInstance(category.id), category.name);
        }
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        //end of Demo
//        getCategory();

    }

    public void getCategory() {
        AndroidNetworking.get(MainApi.GET_CATEGORY)
                .addHeaders("Authorization", "Bearer Fyt_5m1Jx9XfXHAgAdJ2VU54RCMA3QUj")
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            adapter = new TabViewPagerAdapter(getSupportFragmentManager());
                            JSONArray jsonArray = jsonObject.getJSONArray("categories");
                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject object = jsonArray.getJSONObject(i);
//                                Category category = new Category();
//                                category.id = object.getInt("id");
//                                category.name = object.getString("name");
//                                category.created_at = object.getLong("created_at");
//                                category.updated_at = object.getLong("updated_at");
//                                categoryArrayList.add(category);
//                                adapter.addFragment(HomeFragment.newInstance(category.id), category.name);
                            }
                            viewPager.setAdapter(adapter);
                            tabLayout.setupWithViewPager(viewPager);
                        } catch (Exception e) {

                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("error", anError.getErrorDetail());
                    }
                });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_check_out) {
            Bundle bundle = new Bundle();
            bundle.putInt("id", tableId);
            Intent intent = new Intent(HomeActivity.this, ConfirmOrderActivity.class);
            intent.putExtras(bundle);

            startActivity(intent);
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
