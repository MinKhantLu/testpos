package com.devhousemyanmar.pos.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.devhousemyanmar.pos.R;
import com.devhousemyanmar.pos.Util.MainApi;
import com.devhousemyanmar.pos.adapters.TableAdapter;
import com.devhousemyanmar.pos.models.Table;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class TableActivity extends AppCompatActivity implements TableAdapter.ClickTable {

    RecyclerView recyclerView;
    TableAdapter tableAdapter;
    CardView cvTakeAway;
    Boolean flag = false;
    private ArrayList<Table> tables = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);

        recyclerView = findViewById(R.id.rv_table);
        cvTakeAway = findViewById(R.id.cv_take_away);

        cvTakeAway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("title", "Take Away");
                b.putInt("id", 0);
                Intent intent = new Intent(TableActivity.this, HomeActivity.class);
                intent.putExtras(b);
                startActivity(intent);
            }
        });


        //adding demo Data
        tables.clear();
        for (int i = 0; i < 10; i++) {
            tables.add(new Table(i, 0, "A" + i));
        }
        //end of DemoData

        tableAdapter = new TableAdapter(this, this, tables);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView.setAdapter(tableAdapter);
//        getTables();
    }

    @Override
    public void onClickTable(final Table table) {

        if (table.isOccupied == 0) {
            flag = false;
            changeActivity(flag, table.name, table.id);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(table.name);
            View v = LayoutInflater.from(this).inflate(R.layout.dialog_table, null, false);
            builder.setView(v);
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();

            Button btnOrder = v.findViewById(R.id.btn_order);
            Button btnCheckout = v.findViewById(R.id.btn_checkout);

            btnCheckout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    flag = true;
                    alertDialog.dismiss();
                    changeActivity(flag, table.name, table.id);
                }
            });

            btnOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    flag = false;
                    alertDialog.dismiss();
                    changeActivity(flag, table.name, table.id);
                }
            });
        }

    }

    public void changeActivity(Boolean flag, String tableName, int id) {
        if (flag) {
            Bundle b = new Bundle();
            b.putString("title", tableName);
            Intent intent = new Intent(TableActivity.this, CheckOutActivity.class);
            intent.putExtras(b);
            startActivity(intent);
        } else {
            Bundle b = new Bundle();
            b.putString("title", tableName);
            b.putInt("id", id);
            Intent intent = new Intent(TableActivity.this, HomeActivity.class);
            intent.putExtras(b);
            startActivity(intent);
        }
    }

    public void getTables() {
        AndroidNetworking.get(MainApi.GET_TABLE)
                .addHeaders("Authorization", "Bearer Fyt_5m1Jx9XfXHAgAdJ2VU54RCMA3QUj")
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("tables");
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                JSONObject object = jsonArray.getJSONObject(i);
//                                Table table = new Table();
//                                table.id = object.getInt("id");
//                                table.isOccupied = object.getInt("is_occupied");
//                                table.name = object.getString("name");
//                                tables.add(table);
//                            }
                            tableAdapter.notifyDataSetChanged();
                        } catch (Exception e) {


                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}
