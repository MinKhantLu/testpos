package com.devhousemyanmar.pos.activities;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.devhousemyanmar.pos.R;
import com.devhousemyanmar.pos.adapters.OrderAdapter;
import com.devhousemyanmar.pos.models.Order;
import com.devhousemyanmar.pos.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class ConfirmOrderActivity extends AppCompatActivity implements OrderAdapter.ClickOrder {

    RecyclerView recyclerView;
    OrderAdapter orderAdapter;
    Button btnAdd, btnCancel, btnOrder;
    private ArrayList<Order> orderArrayList = new ArrayList<>();
    private Realm realm;
    private int tableId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        tableId = bundle.getInt("id");

        recyclerView = findViewById(R.id.rv_confirm_order);
        btnAdd = findViewById(R.id.btn_add);
        btnCancel = findViewById(R.id.btn_cancel);
        btnOrder = findViewById(R.id.btn_order);
        realm = Realm.getDefaultInstance();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderProducts();
            }
        });

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearRealm();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        orderAdapter = new OrderAdapter(this, this, orderArrayList);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(orderAdapter);

        getOrder();

    }

    @Override
    public void onClickOrder(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View v = LayoutInflater.from(this).inflate(R.layout.dialog_add_product, null, false);
        ImageView imgAdd = v.findViewById(R.id.img_add);
        ImageView imgSub = v.findViewById(R.id.img_sub);
        final TextView txtQuality = v.findViewById(R.id.txt_quality);

        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quality = Integer.parseInt(txtQuality.getText().toString());
                quality++;
                txtQuality.setText(Integer.toString(quality));
            }
        });

        imgSub.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                txtQuality.setText("1");
                return false;
            }
        });

        imgSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quality = Integer.parseInt(txtQuality.getText().toString());
                if (quality > 1) {
                    quality--;
                    txtQuality.setText(Integer.toString(quality));
                }

            }
        });


        builder.setView(v);
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void orderProducts() {

        try {
            User user = realm.where(User.class).findFirst();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", "111");
            jsonObject.put("table_id", tableId);
            jsonObject.put("waiter_id", user.id);
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < orderArrayList.size(); i++) {
                JSONObject orderObj = new JSONObject();
                orderObj.put("id", orderArrayList.get(i).id);
                orderObj.put("name", orderArrayList.get(i).name);
                orderObj.put("quality", orderArrayList.get(i).quality);
                orderObj.put("note", orderArrayList.get(i).note);
                jsonArray.put(i, orderObj);
            }
            jsonObject.put("products", jsonArray);
            Log.e("jsonString", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void clearRealm() {
        RealmResults<Order> results = realm.where(Order.class).findAll();
        realm.beginTransaction();
        results.deleteAllFromRealm();
        realm.commitTransaction();
        onBackPressed();

    }

    public void getOrder() {
        realm.beginTransaction();
        RealmResults<Order> results = realm.where(Order.class).findAll();
        realm.commitTransaction();
        orderArrayList.addAll(results);
        orderAdapter.notifyDataSetChanged();
    }
}
