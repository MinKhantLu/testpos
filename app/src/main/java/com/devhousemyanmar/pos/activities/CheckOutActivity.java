package com.devhousemyanmar.pos.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.devhousemyanmar.pos.R;
import com.devhousemyanmar.pos.adapters.CheckOutAdapter;
import com.devhousemyanmar.pos.adapters.TaxAdapter;

public class CheckOutActivity extends AppCompatActivity implements TaxAdapter.ClickTax {

    RecyclerView recyclerView;
    CheckOutAdapter adapter;
    Button btnPromo, btnTax, btnCheckout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        recyclerView = findViewById(R.id.rv_check_out);
        btnPromo = findViewById(R.id.btn_promo);
        btnTax = findViewById(R.id.btn_tax);
        Bundle b = getIntent().getExtras();
        String title = b.getString("title");
        getSupportActionBar().setTitle(title);

        btnTax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View v = LayoutInflater.from(CheckOutActivity.this).inflate(R.layout.dialog_tax, null, false);
                RecyclerView recyclerView = v.findViewById(R.id.rv_tax);
                TaxAdapter taxAdapter = new TaxAdapter(getApplicationContext());
                recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                recyclerView.setAdapter(taxAdapter);
                AlertDialog.Builder builder = new AlertDialog.Builder(CheckOutActivity.this);
                builder.setView(v);
                builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        adapter = new CheckOutAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        btnPromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View v = LayoutInflater.from(CheckOutActivity.this).inflate(R.layout.dialog_tax, null, false);
                RecyclerView recyclerView = v.findViewById(R.id.rv_tax);
                TaxAdapter taxAdapter = new TaxAdapter(getApplicationContext());
                recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                recyclerView.setAdapter(taxAdapter);
                AlertDialog.Builder builder = new AlertDialog.Builder(CheckOutActivity.this);
                builder.setView(v);
                builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }

    @Override
    public void onClickTax(int position) {

    }
}
